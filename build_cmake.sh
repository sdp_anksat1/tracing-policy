#!/bin/bash

CMAKE=cmake3

do_cmake_for_std() {
    dir="$1"

    mkdir ${dir}
    local status=$?
    if [ ${status} -ne 0 ]; then echo -e "\033[0;31mCreating directory ${dir} failed\033[0m"; return ${status}; fi

    cd ${dir}

    export STDCXX=${std}
    ${CMAKE} ../..
    status=$?
    if [ ${status} -ne 0 ]; then echo -e "\033[0;31mcmake failed for ${dir}\033[0m"; fi

    cd - > /dev/null
    return ${status}
}

do_build_for_std() {
    std="$1"
    local status=0
    echo -e "\033[0;33m\nBuilding for c++$std\033[0m"

    dir="Build_cpp"${std}
    if [ ! -d ${dir} ]; then
        do_cmake_for_std ${dir}
        status=$?
        if [ ${status} -ne 0 ]; then return ${status}; fi
    fi

    cd ${dir}
    make
    status=$?
    cd - > /dev/null
    return ${status}
}

do_build() {
    TARGET_CXX_STANDARDS="$1"
    for std in ${TARGET_CXX_STANDARDS}
    do
        do_build_for_std ${std}
    done
}

do_test_for_std() {
    local std="$1"
    local targets="$2"
    echo -e "\033[0;33m\nExecuting tests for c++${std}\033[0m"

    local dir="Build_cpp"${std}
    if [ ! -d ${dir} ]; then
        echo -e "\033[0;31m${dir} does not exist\033[0m"
        return 1
    fi

    local status=0
    cd ${dir}
    for exe in ${targets}; do
        echo "Executing ${exe}"
        ${exe}
        local prev_status=$?
        if [ ${prev_status} -ne 0 ]; then status=${prev_status}; fi
    done
    cd - > /dev/null
    return ${status}
}

do_test() {
    TARGET_CXX_STANDARDS="$1"
    TARGETS="./test_algorithm ./test_network ./test_list_filter ./test_filter"

    for std in ${TARGET_CXX_STANDARDS}; do
        do_test_for_std ${std} "${TARGETS}"
    done
}

do_build_test() {
    local target_cxx_standards="$1"
    local targets="$2"
    local status=0
    local prev_status=0

    for std in ${target_cxx_standards}; do
        do_build_for_std ${std}
        prev_status=$?
        if [ ${prev_status} -ne 0 ]; then echo -e "\033[0;31mBuild failed for c++${std}\033[0m"; status=${prev_status}; fi

        do_test_for_std ${std} "${targets}"
        prev_status=$?
        if [ ${prev_status} -ne 0 ]; then echo -e "\033[0;31mTests failed for c++${std}\033[0m"; status=${prev_status}; fi
    done
    return ${status}
}

TARGET_CXX_STANDARDS="11 14 17 20"
TARGETS="./test_algorithm ./test_network ./test_list_filter ./test_filter ./test_example"

if [ $# -ge 1 ]; then
    TARGET_CXX_STANDARDS="$1";
fi

#do_build "${TARGET_CXX_STANDARDS}"
#do_test "${TARGET_CXX_STANDARDS}"
do_build_test "${TARGET_CXX_STANDARDS}" "${TARGETS}"

EXIT_CODE=$?
[ ${EXIT_CODE} -ne 0 ] && echo -e "\033[0;31mErrors were encountered\033[0m"
exit ${EXIT_CODE}
