#!/bin/bash

compile_one() {
    OPT_STDCPPVER="$1"
    OPT_OPT="$2"
    OPT_DEBUG="$3"
    OPT_WARNING_FLAGS="$4"
    OPT_INCLUDE_DIRS="$5"
    HEADER_FILE=$6

    #echo "1=$1 2=$2 3=$3 4=$4 5=$5"
    echo "Compiling ${HEADER_FILE}"
    g++ ${OPT_WARNING_FLAGS} ${OPT_STDCPPVER} ${OPT_OPT} ${OPT_DEBUG} ${OPT_INCLUDE_DIRS} -c ${HEADER_FILE}
}

build_all() {
    OPT_STDCPPVER="$1"
    OPT_OPT="$2"
    OPT_DEBUG="$3"
    OPT_WARNING_FLAGS="$4"
    OPT_INCLUDE_DIRS="$5"

    compile_one "$1" "$2" "$3" "$4" "$5" "test/test_framework.hpp"

    compile_one "$1" "$2" "$3" "$4" "$5" "utils/algorithm.hpp"
    echo "Building test_algorithm"
    g++ -Wall -Werror -Wpedantic ${OPT_STDCPPVER} ${OPT_OPT} ${OPT_DEBUG} ${OPT_INCLUDE_DIRS} test/test_framework.hpp test/test_algorithm.cpp -o test_algorithm

    compile_one "$1" "$2" "$3" "$4" "$5" "utils/network.hpp"
    echo "Building test_network"
    g++ -Wall -Werror -Wpedantic ${OPT_STDCPPVER} ${OPT_OPT} ${OPT_DEBUG} ${OPT_INCLUDE_DIRS} test/test_framework.hpp test/test_network.cpp -o test_network

    compile_one "$1" "$2" "$3" "$4" "$5" "inc/list_filter.hpp"
    echo "Building test_list_filter"
    g++ -Wall -Werror -Wpedantic ${OPT_STDCPPVER} ${OPT_OPT} ${OPT_DEBUG} ${OPT_INCLUDE_DIRS} utils/algorithm.hpp inc/list_filter.hpp test/test_framework.hpp test/test_list_filter.cpp -o test_list_filter

    compile_one "$1" "$2" "$3" "$4" "$5" "inc/filter.hpp"
    echo "Building test_filter"
    g++ -Wall -Werror -Wpedantic ${OPT_STDCPPVER} ${OPT_OPT} ${OPT_DEBUG} ${OPT_INCLUDE_DIRS} utils/algorithm.hpp inc/list_filter.hpp test/test_framework.hpp inc/filter.hpp test/test_filter.cpp -o test_filter
}

test_one() {
    BINARY=$1
    echo "Executing ${BINARY}"
    ${BINARY}
    if [ "$?" -eq 0 ]; then
        echo -e "${BINARY}: \033[0;32mPASS\033[0m"
    else
        echo -e "${BINARY}: \033[0;31mFAIL\033[0m"
    fi
}

test_all() {
    test_one ./test_algorithm
    test_one ./test_network
    test_one ./test_list_filter
    test_one ./test_filter
}

build_test_all() {
    echo -e "\033[0;33m Build and test for $1 \033[0m"
    build_all "$1" "$2" "$3" "$4" "$5"
    test_all
    if [ "$?" -ne 0 ]; then
        exit $?
    fi
    echo ""
}

OPT_OPT="-Og"
OPT_DEBUG="-g3"
OPT_WARNING_FLAGS="-Wall -Werror -Wpedantic"
OPT_INCLUDE_DIRS="-I. -I./inc -I./utils -I./test"

if [ "$#" -ge 1 ]; then
    if [[ "$1" = "-h" || "$1" = "--help" || "$1" = "-?" ]]; then
        echo -e "Usage:\n\t$0 [C++-std-option] [optimizaion-flag] [debug-flag] [warning-flags]"
        echo -e "Sample:\n\t$0 -std=c++20 -O2 -g3 \"-Wall -Werror -Wpedantic\""
        echo ""
        echo -e "To compile for all C++ standards, invoke without command-line:"
        echo -e "\n\t$0"
        exit
    fi
    OPT_STDCPPVER=$1

    if [ "$#" -ge 2 ]; then
        OPT_OPT=$2
    fi

    if [ "$#" -ge 3 ]; then
        OPT_DEBUG=$3
    fi

    if [ "$#" -ge 4 ]; then
        OPT_WARNING_FLAGS="$4"
    fi

    #OPT_INCLUDE_DIRS="-I. -I./inc -I./utils -I./test"

    build_test_all "${OPT_STDCPPVER}" "${OPT_OPT}" "${OPT_DEBUG}" "${OPT_WARNING_FLAGS}" "${OPT_INCLUDE_DIRS}"
    exit
fi

OPT_STDCPPVER="-std=c++11"
build_test_all "${OPT_STDCPPVER}" "${OPT_OPT}" "${OPT_DEBUG}" "${OPT_WARNING_FLAGS}" "${OPT_INCLUDE_DIRS}"
OPT_STDCPPVER="-std=c++14"
build_test_all "${OPT_STDCPPVER}" "${OPT_OPT}" "${OPT_DEBUG}" "${OPT_WARNING_FLAGS}" "${OPT_INCLUDE_DIRS}"
OPT_STDCPPVER="-std=c++17"
build_test_all "${OPT_STDCPPVER}" "${OPT_OPT}" "${OPT_DEBUG}" "${OPT_WARNING_FLAGS}" "${OPT_INCLUDE_DIRS}"
OPT_STDCPPVER="-std=c++20"
build_test_all "${OPT_STDCPPVER}" "${OPT_OPT}" "${OPT_DEBUG}" "${OPT_WARNING_FLAGS}" "${OPT_INCLUDE_DIRS}"
