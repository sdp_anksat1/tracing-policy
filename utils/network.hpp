#ifndef EXFO_NETWORK_HPP
#define EXFO_NETWORK_HPP

#include <type_traits>
#include <bit>
#include <algorithm>
#include <span>
#include <array>
#include <cstdint>

namespace exfo
{
    #ifdef __cpp_lib_byteswap
    using std::byteswap;
    #elif __cpp_lib_bit_cast
    template<typename T>
    [[nodiscard]] constexpr T byteswap(T value) noexcept {
        static_assert(std::has_unique_object_representations_v<T>, "T may not have padding bits");
        auto value_representation = std::bit_cast<std::array<std::byte, sizeof(T)>>(value);
        std::ranges::reverse(value_representation);
        return std::bit_cast<T>(value_representation);
    }
    #else
    template<typename T>
    [[nodiscard]] constexpr T byteswap(T value) noexcept {
        #if __cpp_lib_has_unique_object_representations
        static_assert(std::has_unique_object_representations_v<T>, "T may not have padding bits");
        #endif
        //auto& value_representation = reinterpret_cast<std::array<std::byte, sizeof(T)>>(value);
        union
        {
            T value;
            #if __cpp_lib_byte
            std::array<std::byte, sizeof(T)> arr;
            #else
            std::array<uint8_t, sizeof(T)> arr;
            #endif
        } value_representation;
        value_representation.value = value;
        //std::uint8_t* bptr = reinterpret_cast<std::uint8_t*>(&value);
        //std::reverse_copy(bptr, bptr + sizeof(T), begin(value_representation));
        //value = reinterpret_cast<T>(value_representation);
        #ifdef __cpp_lib_ranges
            std::ranges::reverse(value_representation.arr);
        #else
            std::reverse(begin(value_representation.arr), end(value_representation.arr));
        #endif
        return value_representation.value;
    }
    #endif
}

#endif //EXFO_NETWORK_HPP