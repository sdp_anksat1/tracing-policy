#ifndef EXFO_ALGORITHM_HPP
#define EXFO_ALGORITHM_HPP

#include <algorithm>
#include <functional>

namespace exfo
{
    //TODO: execution policy & compare function not handled
    template<typename Container>
    [[nodiscard]] Container sort(Container&& c) {
        #if __cpp_lib_ranges
            std::ranges::sort(c);
        #else
            std::sort(begin(c), end(c));
        #endif
        return c;
    }

    template <typename Iter>
    //requires std::is_iterator
    [[nodiscard]] Iter advance(Iter it, int n) {
        auto ret = it;
        std::advance(ret, n);
        return ret;
    }

    /*  Expects the Container to be sorted already 
        Removes adjacent duplicate entries */
    template <typename Container>
    void dedup_sorted(Container &c) {
        using namespace std::placeholders;
        auto start_it = begin(c);
        auto start_pos = std::distance(begin(c), end(c));
        for (decltype(start_it) found_it;
             (found_it = std::adjacent_find(start_it, end(c))) != end(c);
             start_it = exfo::advance(begin(c), start_pos))
        {
            auto remove_start = exfo::advance(found_it, 1);
            start_pos = std::distance(begin(c), remove_start);
            //This did not work with C++17!
            //auto not_remove_val = std::bind(std::not_equal_to<>{}, _1, *remove_start);
            //auto remove_end = std::find_if(remove_start, end(c), not_remove_val);
            //Following works only with C++14 or later
            //auto remove_end = std::find_if_not(remove_start, end(c),
            //    [remove_val = (*remove_start)](auto val) { return val == remove_val; });
            auto remove_val = *remove_start;
            auto remove_end = std::find_if_not(remove_start, end(c),
                [remove_val](typename Container::value_type val) { return val == remove_val; });
            c.erase(remove_start, remove_end);
        }
    }
} // namespace exfo

#endif //EXFO_ALGORITHM_HPP