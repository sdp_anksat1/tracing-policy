#include "network.hpp"

#include "test_framework.hpp"

using namespace exfo::ut;

[[nodiscard]] test_stats test_byteswap() {
    test_stats stats;

    stats += require(exfo::byteswap(uint32_t(0x0a0b0c0d)) == uint32_t(0x0d0c0b0a), "byteswap uint32_t", __FILE__, __LINE__, 0, __func__);

    return stats;
}

int main(int argc, char** argv) {
    test_stats stats{};

    stats += test_byteswap();

    if (stats.failed) {
        std::cout << "FAILED tests = " << stats.failed << "; Total tests = " << stats.total << '\n';
    }
    return -stats.failed;
}