#include "filter.hpp"
#include "list_filter.hpp"

#include <cstdint>
#include <vector>
#include <string>
#include <iostream>

//All existing constructs that the application already has
namespace probe
{
    enum cdr_status { good, error };

    struct T_CDR_MSG
    {
        cdr_status status_;
        uint64_t imsi_;
        uint16_t src_port_;
        uint16_t dst_port_;
    };
    
    std::ostream& operator<<(std::ostream& out, const T_CDR_MSG& cdr) {
        return out 
            << "{ cdr_status: " << (cdr.status_ == good? "good": "error")
            << ", imsi: " << cdr.imsi_
            << ", src_port: " << cdr.src_port_
            << ", dst_port: " << cdr.dst_port_
            << "}";
    }
} // namespace probe

//New constructs for the interface
namespace probe
{
    //We need a unique data-type for every item we want to filter on - for get_val() to work
    //Using uint64_t is not okay as many other items will have the same data-type

    //Wrapper struct to have a unique data-type for imsi
    struct imsi { uint64_t val; };
    //Mandatory operators for comparison
    bool operator<(const imsi& lhs, const imsi& rhs) noexcept { return lhs.val < rhs.val; }
    bool operator==(const imsi& lhs, const imsi& rhs) noexcept { return lhs.val == rhs.val; }
    
    //Wrapper struct to have a unique data-type for imsi
    struct port { uint16_t val; };
    //Mandatory operators for comparison
    bool operator<(const port& lhs, const port& rhs) noexcept { return lhs.val < rhs.val; }
    bool operator==(const port& lhs, const port& rhs) noexcept { return lhs.val == rhs.val; }
    
    //Implement get_val for each data-type we need filtering on
    template<typename T>
    [[nodiscard]] T get_val(const T_CDR_MSG& cdr);
    template<typename T>
    [[nodiscard]] T get_val(const T_CDR_MSG& cdr, int occurrence);

    template<> [[nodiscard]] imsi get_val<imsi>(const T_CDR_MSG& cdr) { return {cdr.imsi_}; }
    template<> [[nodiscard]] cdr_status get_val<cdr_status>(const T_CDR_MSG& cdr) { return cdr.status_; }

    template<> [[nodiscard]] port get_val<port>(const T_CDR_MSG& cdr, int occurrence) {
        return {occurrence == 0? cdr.src_port_: cdr.dst_port_};
    }

    //The Filters we use
    using imsi_include_filter = typename exfo::include_list_filter<imsi>;
    using cdr_status_filter = typename exfo::include_list_filter<cdr_status>;
    using port_include_filter = typename exfo::include_list_filter<port, 2>;

#if __cplusplus >= 201703L
    using probe_filter = exfo::rule_t<imsi_include_filter, cdr_status_filter, port_include_filter>;
#else
    using probe_filter = imsi_include_filter;
#endif
    using filters = std::vector<probe_filter>;
} // namespace probe

//Initialization code to create the filters from a string (that comes from ConfigMap)
namespace probe
{
    std::string read_config_map(const std::string& /*config_name*/) {
        return "\"rules\": {"
                    "\"imsi\": [ \"1111111111\", \"2222222222\", \"3333333333\", \"4444444444\"]"
                    "\"status\": [ \"error\" ]"
                    "\"port\": [ \"4060\", \"5060\", \"6060\"]"
                "}";
    }

#if __cplusplus >= 201703L
    filters parse_tracing_filters(const std::string& /*config*/) {
        return {
            imsi_include_filter{{1111111111u}, {2222222222u}, {3333333333u}, {4444444444u}},
            cdr_status_filter{error},
            port_include_filter{{4060u}, {5060u}, {6060u}}
        };
    }
#else
    filters parse_tracing_filters(const std::string& config) {
        return {
            imsi_include_filter{{1111111111u}, {2222222222u}, {3333333333u}, {4444444444u}}
        };
    }
#endif

    T_CDR_MSG process_msg() {
        static int count = 0;
        switch (count++) {
        case 0: return { cdr_status::error, 9999999999u, 8888, 7777 };  //matches cdr_status only
        case 1: return { cdr_status::good, 2222222222u, 8888, 7777 };   //matches imsi only
        case 2: return { cdr_status::good, 9999999999u, 8888, 5060 };   //matches dst_port only
        case 3: return { cdr_status::good, 9999999999u, 8888, 7777 };   //matches none
        default:
            break;
        }
        return { cdr_status::good, 9999999999u, 8888, 7777 };
    }

    void send_to_kafka(const T_CDR_MSG& cdr) {
        //auto encoded_buffer = cves_encoder.encode(cdr);
        //kafka_producer.produce(encoded_buffer);
        std::cout << "CDR sent to Kafka: " << cdr << '\n';
    }
} // namespace probe


int main(int argc, char** argv) {
    using namespace probe;

    //Initialization
    std::string tracing_config = read_config_map("tracing-policy");
    filters tracing_filters = parse_tracing_filters(tracing_config);

    //OUTCOME: 
    //For C++11 & C++14, Selects & Sends only 2nd CDR to Kafka, Drop the other 3
    //For C++17 & C++20: Selects & Sends first 3 CDRs to Kafka, Drops no 4

    //Application event loop
    for (int i = 0; i < 4; ++i) {
        T_CDR_MSG cdr = process_msg();
        if (exfo::is_include(cdr, tracing_filters)) {
            send_to_kafka(cdr);
        } else {
            std::cout << "CDR DROPPED: " << cdr << '\n';
        }
    }
    return 0;
}