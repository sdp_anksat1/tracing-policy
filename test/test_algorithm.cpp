#include "algorithm.hpp"
#include "test_framework.hpp"

#include <iostream>
#include <vector>

using namespace exfo::ut;

[[nodiscard]] test_stats test_dedup_sorted() {
    test_stats res{};
    std::vector<int> sorted_v{1, 1, 2, 2, 2, 3, 3, 4, 5, 5, 6, 7, 7, 7, 7, 8, 9, 9};
    exfo::dedup_sorted(sorted_v);
    res += require(sorted_v == std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9}, "dedup_sorted mismatch expected", __FILE__, __LINE__, 0, __func__);

    sorted_v = std::vector<int>{1, 4, 4, 4, 4, 4, 4};
    exfo::dedup_sorted(sorted_v);
    res += require(sorted_v == std::vector<int>{1, 4}, "dedup_sorted mismatch expected for tail repetition", __FILE__, __LINE__, 0, __func__);

    sorted_v = std::vector<int>{4, 4, 4, 4, 4, 4, 5};
    exfo::dedup_sorted(sorted_v);
    res += require(sorted_v == std::vector<int>{4, 5}, "dedup_sorted mismatch expected for head repetition", __FILE__, __LINE__, 0, __func__);

    sorted_v = std::vector<int>{4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
    exfo::dedup_sorted(sorted_v);
    res += require(sorted_v == std::vector<int>{4}, "dedup_sorted mismatch expected for one repeated value", __FILE__, __LINE__, 0, __func__);

    return res;
}

int main(int argc, char** argv) {
    test_stats stats{};

    stats += test_dedup_sorted();

    if (stats.failed) {
        std::cout << "FAILED tests = " << stats.failed << "; Total tests = " << stats.total << '\n';
    }
    return -stats.failed;
}