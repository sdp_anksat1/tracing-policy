#include "list_filter.hpp"

#include "algorithm.hpp"
#include "test_framework.hpp"

#include <iostream>
#include <type_traits>
#include <string>

#if __cpp_lib_string_udls
using namespace std::string_literals;
#endif

//constexpr auto x = exfo::list_filter<int, 1, true>{11, 22, 33, 44}.get_list();
//static_assert(exfo::list_filter<int, 1, true>{11, 22, 33, 44}.get_list().size() == 4);

using namespace exfo::ut;

[[nodiscard]] test_stats test_int_exclude_filter_iter() {
    test_stats res{};
    std::vector<int> expected{99, 88, 77, 66, 55, 44, 33, 22, 11};
    exfo::exclude_list_filter<int> filter(begin(expected), end(expected));
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, int>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(5000) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(55) == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 9, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == exfo::sort(expected), "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_int_exclude_filter_vector() {
    test_stats res{};
    std::vector<int> expected{99, 88, 77, 66, 55, 44, 33, 22, 11};
    auto copy_vec{expected};
    exfo::exclude_list_filter<int, 1> filter(std::move(copy_vec));
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, int>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(5000) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(55) == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 9, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == exfo::sort(expected), "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_int_exclude_filter_init_list() {
    test_stats res{};
    exfo::list_filter<int, 1, false> filter{11, 22, 33, 44, 55};
    res += require(exfo::list_filter<int, 1, true>::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, int>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(99) == true, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(55) == false, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 5, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == std::vector<int>{11, 22, 33, 44, 55}, "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_int_include_filter_iter() {
    test_stats res{};
    std::vector<int> expected{99, 88, 77, 66, 55, 44, 33, 22, 11};
    exfo::include_list_filter<int> filter(begin(expected), end(expected));
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, int>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(5000) == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(55) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 9, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == exfo::sort(expected), "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_int_include_filter_vector() {
    test_stats res{};
    std::vector<int> expected{99, 88, 77, 66, 55, 44, 33, 22, 11};
    auto copy_vec{expected};
    exfo::include_list_filter<int, 1> filter(std::move(copy_vec));
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, int>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(5000) == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(55) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(22) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 9, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == exfo::sort(expected), "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_int_include_filter_init_list() {
    test_stats res{};
    exfo::list_filter<int, 1, true> filter{11, 22, 33, 44, 55};
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, int>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(99) == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include(55) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 5, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == std::vector<int>{11, 22, 33, 44, 55}, "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_string_include_filter_init_list() {
    test_stats res{};
    exfo::list_filter<std::string> filter{"alpha", "beta", "gamma", "delta", "theeta", "lambda"};
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, std::string>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include("absent") == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include("beta") == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 6, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == exfo::sort(std::vector<std::string>{"alpha", "beta", "gamma", "delta", "theeta", "lambda"}), "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_string_include_filter_vector() {
    test_stats res{};
    std::vector<std::string> expected{"alpha", "beta", "gamma", "delta", "theeta", "lambda"};
    auto copy_vec{expected};
    exfo::include_list_filter<std::string, 1> filter(std::move(copy_vec));
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, std::string>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include("EXFO") == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include("alpha") == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include("theeta") == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 6, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == exfo::sort(expected), "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_string_include_filter_iter() {
    test_stats res{};
    std::vector<std::string> expected{"alpha", "beta", "gamma", "delta", "theeta", "lambda"};
    exfo::include_list_filter<std::string> filter(begin(expected), end(expected));
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, std::string>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include("XXX") == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include("delta") == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 6, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == exfo::sort(expected), "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    //std::copy(begin(filter.get_list()), end(filter.get_list()), std::ostream_iterator<std::string>(std::cout, ", "));
    //std::cout << '\n';
    return res;
}

namespace ns_test_list_filter
{
    struct imsi
    {
        uint64_t val;
    };
    bool operator==(const imsi& lhs, const imsi& rhs) noexcept { return lhs.val == rhs.val; }
    bool operator<(const imsi& lhs, const imsi& rhs) noexcept { return lhs.val < rhs.val; }

    struct port
    {
        uint16_t val;
    };
    bool operator==(const port& lhs, const port& rhs) noexcept { return lhs.val == rhs.val; }
    bool operator<(const port& lhs, const port& rhs) noexcept { return lhs.val < rhs.val; }

    struct cdr
    {
        imsi imsi_;
        port src_port_;
        port dst_port_;
    };
}
using namespace ns_test_list_filter;

[[nodiscard]] test_stats test_imsi_include_filter_init_list() {
    test_stats res{};
    exfo::list_filter<imsi> filter{imsi{2222u}, {1111u}, {3333u}, {1111u}, {5555u}, {1111u}, {4444u}, {1111u}};
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, imsi>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include({123456789u}) == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include({4444u}) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 5, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list() == std::vector<imsi>{imsi{1111u}, {2222u}, {3333u}, {4444u}, {5555u}}, "filter get_list contents sorted and dedup'ed", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_imsi_include_filter_vector() {
    test_stats res{};
    std::vector<imsi> expected{{202020u}, {505050u}, {101010u}, {303030u}, {404040u}};
    auto copy_vec{expected};
    exfo::include_list_filter<imsi, 1> filter(std::move(copy_vec));
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, imsi>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include({909090u}) == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include({404040u}) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include({202020u}) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 5, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    std::sort(begin(expected), end(expected));
    res += require(filter.get_list() == expected, "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    //res += require(filter.get_list() == exfo::sort(expected), "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_imsi_include_filter_iter() {
    test_stats res{};
    std::vector<imsi> expected{{77777u}, {88888u}, {33333u}, {22222u}, {66666u}, {44444u}, {11111u}, {55555u}};
    exfo::include_list_filter<imsi> filter(begin(expected), end(expected));
    res += require(decltype(filter)::values_count == 1, "filter values_count", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename decltype(filter)::value_type, imsi>::value, "filter value_type", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include({123123u}) == false, "filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.should_include({44444u}) == true, "filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(filter.get_list().size() == 8, "filter get_list size", __FILE__, __LINE__, 0, __func__);
    std::sort(begin(expected), end(expected));
    res += require(filter.get_list() == expected, "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    //res += require(filter.get_list() == exfo::sort(expected), "filter get_list contents", __FILE__, __LINE__, 0, __func__);
    //std::copy(begin(filter.get_list()), end(filter.get_list()), std::ostream_iterator<std::string>(std::cout, ", "));
    //std::cout << '\n';
    return res;
}

[[nodiscard]] test_stats test_should_include_empty_filter() {
    test_stats res{};
    res += require(exfo::include_list_filter<int>().should_include(10) == false, "Empty include filter allows all", __FILE__, __LINE__, 0, __func__);
    res += require(exfo::include_list_filter<double, 2>().should_include(3.1415) == false, "Empty include filter allows all", __FILE__, __LINE__, 0, __func__);
    res += require(exfo::exclude_list_filter<imsi>().should_include({334455u}) == true, "Empty include filter allows all", __FILE__, __LINE__, 0, __func__);
    res += require(exfo::exclude_list_filter<imsi, 2>().should_include({1111u}) == true, "Empty include filter allows all", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_list_filter_value_type() {
    test_stats res{};
    res += require(std::is_same<typename exfo::include_list_filter<int>::value_type, int>::value, "Correct include_list_filter<int>::value_type", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename exfo::include_list_filter<double, 2>::value_type, double>::value, "Correct include_list_filter<double, 2>::value_type", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename exfo::exclude_list_filter<imsi>::value_type, imsi>::value, "Correct exclude_list_filter<imsi>::value_type", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename exfo::exclude_list_filter<std::pair<int, imsi>, 3>::value_type, std::pair<int, imsi>>::value, "Correct exclude_list_filter<std::pair<int, imsi>, 3>::value_type", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename exfo::list_filter<port, 4, true>::value_type, port>::value, "Correct list_filter<port, 4, true>::value_type", __FILE__, __LINE__, 0, __func__);
    res += require(std::is_same<typename exfo::list_filter<uint16_t, 6, false>::value_type, uint16_t>::value, "Correct list_filter<uint16_t, 6, false>::value_type", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_list_filter_values_count() {
    test_stats res{};
    res += require(exfo::include_list_filter<int>::values_count == 1, "Correct include_list_filter<int>::values_count", __FILE__, __LINE__, 0, __func__);
    res += require(exfo::include_list_filter<double, 2>::values_count == 2, "Correct include_list_filter<double, 2>::values_count", __FILE__, __LINE__, 0, __func__);
    res += require(exfo::exclude_list_filter<imsi>::values_count == 1, "Correct exclude_list_filter<imsi>::values_count", __FILE__, __LINE__, 0, __func__);
    res += require(exfo::exclude_list_filter<std::pair<int, imsi>, 3>::values_count == 3, "Correct exclude_list_filter<std::pair<int, imsi>, 3>::values_count", __FILE__, __LINE__, 0, __func__);
    res += require(exfo::list_filter<port, 4, true>::values_count == 4, "Correct list_filter<port, 4, true>::values_count", __FILE__, __LINE__, 0, __func__);
    res += require(exfo::list_filter<uint16_t, 6, false>::values_count == 6, "Correct list_filter<uint16_t, 6, false>::values_count", __FILE__, __LINE__, 0, __func__);
    return res;
}

[[nodiscard]] test_stats test_list_filter_construction() {
    test_stats res{};

    exfo::list_filter<imsi> imsi_unordered{imsi{2222u}, {1111u}, {3333u}, {5555u}, {4444u}};
    res += require(imsi_unordered.get_include_list().size() == 5, "filter get_include_list size", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_unordered.get_exclude_list().size() == 0, "filter get_exclude_list empty", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_unordered.get_include_list() == std::vector<imsi>{imsi{1111u}, {2222u}, {3333u}, {4444u}, {5555u}}, "filter get_include_list contents sorted", __FILE__, __LINE__, 0, __func__);

    exfo::list_filter<imsi, 2> imsi_unordered_duplicate{
        std::vector<imsi>{imsi{2222u}, {1111u}, {3333u}, {1111u}, {5555u}, {1111u}, {4444u}, {1111u}}};
    res += require(imsi_unordered_duplicate.get_include_list().size() == 5, "filter get_include_list size", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_unordered_duplicate.get_exclude_list().size() == 0, "filter get_include_list empty", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_unordered_duplicate.get_include_list() == std::vector<imsi>{imsi{1111u}, {2222u}, {3333u}, {4444u}, {5555u}}, "filter get_include_list contents sorted and dedup'ed", __FILE__, __LINE__, 0, __func__);

    std::vector<std::string> strs{"fifth", "fifth", "first", "fourth", "second", "second", "second", "sixth", "sixth", "third", "third"};
    exfo::list_filter<std::string, 3, true> str_ordered_duplicate{begin(strs), end(strs)};
    res += require(str_ordered_duplicate.get_include_list().size() == 6, "filter get_include_list size", __FILE__, __LINE__, 0, __func__);
    res += require(str_ordered_duplicate.get_exclude_list().size() == 0, "filter get_exclude_list empty", __FILE__, __LINE__, 0, __func__);
    res += require(str_ordered_duplicate.get_include_list() == 
        std::vector<std::string>{"fifth", "first", "fourth", "second", "sixth", "third"}, "filter get_include_list contents dedup'ed", __FILE__, __LINE__, 0, __func__);

    exfo::list_filter<std::string, 10, false> exclude_str_ordered_duplicate{std::move(strs)};
    res += require(exclude_str_ordered_duplicate.get_exclude_list().size() == 6, "filter get_exclude_list size", __FILE__, __LINE__, 0, __func__);
    res += require(exclude_str_ordered_duplicate.get_include_list().size() == 0, "filter get_include_list empty", __FILE__, __LINE__, 0, __func__);
    res += require(exclude_str_ordered_duplicate.get_exclude_list() == 
        std::vector<std::string>{"fifth", "first", "fourth", "second", "sixth", "third"}, "filter get_exclude_list contents dedup'ed", __FILE__, __LINE__, 0, __func__);

    exfo::exclude_list_filter<int> exclude_empty{};
    res += require(exclude_empty.get_exclude_list().size() == 0, "filter get_exclude_list empty", __FILE__, __LINE__, 0, __func__);
    res += require(exclude_empty.get_include_list().size() == 0, "filter get_include_list empty", __FILE__, __LINE__, 0, __func__);

    return res;
}

namespace exfo
{
    //template<typename T>
    //using interval = std::pair<T, T>;
    template<typename T>
    struct interval
    {
        T first;
        T second;
    };

    //type_traits
    template<typename T>
    struct is_interval : public std::false_type {};
    template<typename T>
    struct is_interval<exfo::interval<T>> : public std::true_type {};
    //template<typename T>
    //using is_interval_v = is_interval<T>::value;

    static_assert(is_interval<interval<int>>::value, "is_interval");
    static_assert(!is_interval<int>::value, "not is_interval");

    template<typename T>
    bool contains(const interval<T>& rng, const T& val) noexcept {
        //return rng.first <= val && val <= rng.second;
        return (rng.first < val || rng.first == val) && (val < rng.second || val == rng.second);
    }

    template<typename T>
    bool operator<(const interval<T>& lhs, const interval<T>& rhs) noexcept { return lhs.second < rhs.first; }
    template<typename T>
    bool operator==(const interval<T>& lhs, const interval<T>& rhs) noexcept {   //lhs == rhs OR rhs lies within lhs
        return contains(lhs, rhs.first) && contains(lhs, rhs.second);
        //(lhs.first == rhs.first || lhs.first < rhs.first ) && (rhs.second < lhs.second || rhs.second == lhs.second);
    }

    template<typename T>
    bool operator<(const interval<T>& lhs, const T& val) noexcept { return lhs.second < val; }
    template<typename T>
    bool operator<(const T& val, const interval<T>& rhs) noexcept { return val < rhs.first; }
    template<typename T>
    bool operator==(const interval<T>& lhs, const T& val) noexcept { return contains(lhs, val); }
    template<typename T>
    bool operator==(const T& val, const interval<T>& rhs) noexcept { return contains(rhs, val); }

    struct less
    {
        template<typename T, typename U> bool operator()(const T& t, const U& u) { return t < u; }
    };

    template<typename T, int no_values = 1>
    bool should_include(const exfo::include_list_filter<interval<T>, no_values>& filt, const T& value) {
        const auto& values = filt.get_include_list();
        return std::binary_search(begin(values), end(values), value/*, std::less{}*/);
        //return std::binary_search(begin(values), end(values), value, less{});
    }
}

namespace ns_test_list_filter
{
    using imsi_interval = exfo::interval<imsi>;
    using imsi_interval_include_filter = exfo::include_list_filter<imsi_interval>;

    using int_interval = exfo::interval<int>;
    using int_interval_include_filter = exfo::include_list_filter<int_interval>;
}

//template<typename T>
//bool operator<(const T& val, const interval<T>& rhs) { return val < rhs.first; }

[[nodiscard]] test_stats test_interval_filter() {
    test_stats res{};

    imsi_interval_include_filter imsi_int_filt{{{1000u}, {2000u}}, {{3000u}, {4000u}}, {{5000u}, {6000u}}, {{7000u}, {8000u}}};

    res += require(imsi_int_filt.get_include_list().size() == 4, "imsi_interval_filter get_include_list size", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_int_filt.should_include({{1000u}, {1000u}}) == true, "imsi_interval_filter should_include first interval", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_int_filt.should_include({{5500u}, {5500u}}) == true, "imsi_interval_filter should_include middle interval", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_int_filt.should_include({{4000u}, {4000u}}) == true, "imsi_interval_filter should_include second interval", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_int_filt.should_include({{999u}, {999u}}) == false, "imsi_interval_filter NOT should_include one before first interval", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_int_filt.should_include({{9000u}, {9000u}}) == false, "imsi_interval_filter NOT should_include middle interval", __FILE__, __LINE__, 0, __func__);
    res += require(imsi_int_filt.should_include({{6001u}, {6001u}}) == false, "imsi_interval_filter NOT should_include one after second interval", __FILE__, __LINE__, 0, __func__);

    res += require(should_include(imsi_int_filt, imsi{3000u}) == true, "imsi_interval_filter should_include first value", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(imsi_int_filt, imsi{7777u}) == true, "imsi_interval_filter should_include middle value", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(imsi_int_filt, imsi{6000u}) == true, "imsi_interval_filter should_include second value", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(imsi_int_filt, imsi{2999u}) == false, "imsi_interval_filter NOT should_include one before first value", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(imsi_int_filt, imsi{4444u}) == false, "imsi_interval_filter NOT should_include middle value", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(imsi_int_filt, imsi{8001u}) == false, "imsi_interval_filter NOT should_include one after second value", __FILE__, __LINE__, 0, __func__);

    using port_interval = exfo::interval<port>;
    using port_interval_include_filter = exfo::include_list_filter<port_interval>;
    port_interval_include_filter port_int_filt{port_interval{port{10001}, port{15000}}, {{20001}, {25000}}, {{30001}, {35000}}, {{40001}, {45000}}};
    res += require(port_int_filt.get_include_list().size() == 4, "port_interval_filter get_include_list size", __FILE__, __LINE__, 0, __func__);
    res += require(port_int_filt.should_include({{44000u}, {44000u}}) == true, "port_interval_filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(port_int_filt, port{33000u}) == true, "port_interval_filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(port_int_filt.should_include({{49000u}, {49000u}}) == false, "port_interval_filter should_include", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(port_int_filt, port{37000u}) == false, "port_interval_filter NOT should_include", __FILE__, __LINE__, 0, __func__);

    int_interval_include_filter int_int_filt{int_interval{5001, 15000}, {25001, 35000}, {45001, 55000}, {65001, 75000}};
    res += require(int_int_filt.get_include_list().size() == 4, "int_interval_filter get_include_list size", __FILE__, __LINE__, 0, __func__);
    res += require(int_int_filt.should_include({45001, 45001}) == true, "int_interval_filter should_include match first", __FILE__, __LINE__, 0, __func__);
    res += require(int_int_filt.should_include({52000, 52000}) == true, "int_interval_filter should_include match middle", __FILE__, __LINE__, 0, __func__);
    res += require(int_int_filt.should_include({15000, 15000}) == true, "int_interval_filter should_include match second", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(int_int_filt, 55000) == true, "int_interval_filter should_include first value", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(int_int_filt, 11111) == true, "int_interval_filter should_include middle value", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(int_int_filt, 75000) == true, "int_interval_filter should_include second value", __FILE__, __LINE__, 0, __func__);

    res += require(int_int_filt.should_include({65000, 65000}) == false, "int_interval_filter NOT should_include one less than first", __FILE__, __LINE__, 0, __func__);
    res += require(int_int_filt.should_include({20000, 20000}) == false, "int_interval_filter NOT should_include", __FILE__, __LINE__, 0, __func__);
    res += require(int_int_filt.should_include({35001, 35001}) == false, "int_interval_filter NOT should_include one more than second", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(int_int_filt, 25000) == false, "int_interval_filter NOT should_include one less than first value", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(int_int_filt, 22222) == false, "int_interval_filter NOT should_include value", __FILE__, __LINE__, 0, __func__);
    res += require(should_include(int_int_filt, 15001) == false, "int_interval_filter NOT should_include one greater than second value", __FILE__, __LINE__, 0, __func__);

    return res;
}

int main(int argc, char** argv) {
    test_stats stats{};
    stats += test_int_include_filter_init_list();
    stats += test_int_include_filter_vector();
    stats += test_int_include_filter_iter();

    stats += test_string_include_filter_init_list();
    stats += test_string_include_filter_vector();
    stats += test_string_include_filter_iter();

    stats += test_int_exclude_filter_init_list();
    stats += test_int_exclude_filter_vector();
    stats += test_int_exclude_filter_iter();

    stats += test_imsi_include_filter_init_list();
    stats += test_imsi_include_filter_vector();
    stats += test_imsi_include_filter_iter();

    stats += test_should_include_empty_filter();
    stats += test_list_filter_value_type();
    stats += test_list_filter_values_count();
    stats += test_list_filter_construction();
    stats += test_interval_filter();

    if (stats.failed) {
        std::cout << "FAILED tests = " << stats.failed << "; Total tests = " << stats.total << '\n';
    }
    return -stats.failed;
}