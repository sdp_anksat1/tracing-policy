#ifndef EXFO_TEST_FRAMEWORK_HPP
#define EXFO_TEST_FRAMEWORK_HPP

#include <iostream>
#include <vector>
#if __cpp_lib_source_location
#include <source_location>
#endif
#if __cpp_lib_span
#include <span>
#endif

namespace exfo
{
    namespace ut
    {
        bool require(bool test, const char* msg, const char* file_name, size_t line, size_t column, const char* function_name) {
            if (!test) {
                std::cerr << "FAIL: `" << file_name << ':' << line << ':' << column << " - " << function_name << "` -> " << msg << '\n';
            }
            return test;
        }
        #if __cpp_lib_source_location
        bool require(bool test, auto source = std::source_location::current()) {
            std::cerr << "FAIL: `" << source.file_name() << ':' << source.line() << ':' << source.column << " - " << source.function_name() << "` -> " "<no msg>\n";
        }
        #endif

        struct test_stats
        {
            int failed = 0;
            int total = 0;
        };
        test_stats& operator+=(test_stats& stats, const test_stats other) {
            stats.failed += other.failed;
            stats.total += stats.total;
            return stats;
        }
        test_stats& operator+=(test_stats& stats, bool test) {
            if (!test) ++stats.failed;
            ++stats.total;
            return stats;
        }

        #if __cpp_lib_span
        template<typename T>
        [[nodiscard]] bool operator==(const std::span<const T> a, const std::span<const T> b) {
            return a.size() == b.size() && std::equal(begin(a), end(a), begin(b));
        }
        #else
        template<typename T>
        [[nodiscard]] bool operator==(const std::vector<const T>& a, const std::vector<const T>& b) {
            return a.size() == b.size() && std::equal(begin(a), end(a), begin(b));
        }
        #endif
    }   //namespace ut
}   //namespace exfo

#endif  //EXFO_TEST_FRAMEWORK_HPP