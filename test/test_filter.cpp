#include "filter.hpp"

#include "list_filter.hpp"
#include "test_framework.hpp"

//#include <nlohmann/json.hpp"
#include <chrono>
#include <string>
#include <algorithm>
#include <type_traits>
#include <iostream>

#if __cpp_lib_string_udls
using namespace std::string_literals;
#endif


namespace ns_test_filter_rule 
{
    struct imsi { uint64_t val; };
    bool operator<(const imsi& lhs, const imsi& rhs) noexcept { return lhs.val < rhs.val; }
    bool operator==(const imsi& lhs, const imsi& rhs) noexcept { return lhs.val == rhs.val; }

    enum cdr_status { good, error };

    struct port { uint16_t val; };
    bool operator<(const port& lhs, const port& rhs) noexcept { return lhs.val < rhs.val; }
    bool operator==(const port& lhs, const port& rhs) noexcept { return lhs.val == rhs.val; }

    struct time_point { std::chrono::time_point<std::chrono::system_clock> val; };
    bool operator<(const time_point& lhs, const time_point& rhs) noexcept { return lhs.val < rhs.val; }
    bool operator==(const time_point& lhs, const time_point& rhs) noexcept { return lhs.val == rhs.val; }

    struct T_CDR_MSG
    {
        imsi imsi_;
        cdr_status status_;
        port src_port_;
        port dst_port_;
        time_point start_time_;
        time_point end_time_;
    };

    #if __cpp_if_constexpr
    template<typename T>
    [[nodiscard]] const auto& get_val(const T_CDR_MSG& cdr) {
        if constexpr (std::is_same_v<T, imsi>) { return cdr.imsi_; }
        if constexpr (std::is_same_v<T, cdr_status>) { return cdr.status_; }
    }

    template<typename T>
    [[nodiscard]] const auto& get_val(const T_CDR_MSG& cdr, int occurrence) {
        if constexpr (std::is_same_v<T, port>) {
            if (occurrence == 0) return cdr.src_port_;
            if (occurrence == 1) return cdr.dst_port_;
            thread_local const port null_port{};
            return null_port;
        } else if constexpr (std::is_same_v<T, time_point>) {
            if (occurrence == 0) return cdr.start_time_;
            if (occurrence == 1) return cdr.end_time_;
            thread_local const time_point null_time_point{};
            return null_time_point;
        }
    }
    #else
        template<typename T>
        [[nodiscard]] const T& get_val(const T_CDR_MSG& cdr);
        template<typename T>
        [[nodiscard]] const T& get_val(const T_CDR_MSG& cdr, int occurrence);

        template<>
        [[nodiscard]] const imsi& get_val<imsi>(const T_CDR_MSG& cdr) { return cdr.imsi_; }
        template<>
        [[nodiscard]] const cdr_status& get_val<cdr_status>(const T_CDR_MSG& cdr) { return cdr.status_; }
        template<>
        [[nodiscard]] const port& get_val<port>(const T_CDR_MSG& cdr, int occurrence) {
            if (occurrence == 0) { return {cdr.src_port_}; }
            if (occurrence == 1) { return {cdr.dst_port_}; }
            thread_local const port null_port{};
            return null_port;
        }
        template<>
        [[nodiscard]] const time_point& get_val<time_point>(const T_CDR_MSG& cdr, int occurrence) {
            if (occurrence == 0) { return {cdr.start_time_}; }
            if (occurrence == 1) { return {cdr.end_time_}; }
            thread_local const time_point null_time_point{};
            return null_time_point;
        }
    #endif

    using imsi_include_list_filter = typename exfo::include_list_filter<imsi>;
    using cdr_status_list_filter = typename exfo::include_list_filter<cdr_status>;
    using port_include_list_filter = typename exfo::include_list_filter<port, 2>;

#if __cplusplus >= 201703L
    using rule = exfo::rule_t<imsi_include_list_filter, cdr_status_list_filter>;

    using Rules = std::vector<rule>;
    Rules filter_rules{imsi_include_list_filter{{1111u}, {2222u}, {3333u}, {4444u}}, cdr_status_list_filter{error}};
#endif  //C++17 or higher

    namespace parsing
    {
        enum app_type {SBC, S1, S11};
        app_type this_app_type = SBC;

        struct unknown_rule_type : public std::exception {
            unknown_rule_type(const std::string& type) : msg(std::string{"unknown rule type "} + type) {}
            const char* what() const noexcept override { return msg.c_str(); }
            private:
            std::string msg;
        };
        enum rule_type { imsi_list, status };
        rule_type parse_rule_type(const std::string& rule_str) {
            if (rule_str == "imsi") return imsi_list;
            if (rule_str == "status") return status;
            throw unknown_rule_type(rule_str);
        }

        bool is_rule_type_supported(rule_type type) {
            return this_app_type == SBC && type != imsi_list;    //imsi_list is not supported for SBC; all rules are supported for all others
        }
        template<typename ValueType>
        auto parse_rule_values(const std::vector<uint64_t>& jvals) -> std::vector<ValueType> {
#if __cpp_if_constexpr
            if constexpr (std::is_same_v<ValueType, imsi>)
#else
            if (std::is_same<ValueType, imsi>::value)
#endif
            {
                std::vector<ValueType> values;
                std::transform(begin(jvals), end(jvals), begin(values), [](uint64_t val) { return imsi{val}; } );
                return values;
            }
#if __cpp_if_constexpr
            if constexpr (std::is_same_v<ValueType, cdr_status>)
#else
            if (std::is_same<ValueType, cdr_status>::value)
#endif
            {
                std::vector<ValueType> values{error};
                return values;
            }
        }
        template<typename ValueType>
        auto parse_rule_status_values(const std::vector<uint64_t>&) -> std::vector<ValueType> {
            std::vector<ValueType> values{error};
            return values;
        }

#if __cplusplus >= 201703L
        struct json_rule
        {
            std::string type;
            std::vector<uint64_t> values;
        };
        using json_rules = std::vector<json_rule>;
        //using nlohmann_json = std::map<std::string, std::string>;

        rule parse_rule(const json_rule& rule) {
            switch(parse_rule_type(rule.type)) {
                case imsi_list: return imsi_include_list_filter(parse_rule_values<imsi>(rule.values));
                case status: return cdr_status_list_filter(parse_rule_values<cdr_status>(rule.values));
                default: throw unknown_rule_type(rule.type);
            }
        }
        std::vector<rule> parse_filter_rules(const json_rules& jrs) {
            std::vector<rule> rules;
            for (const auto& r : jrs) {
                if (is_rule_type_supported(parse_rule_type(r.type))) {
                    rules.push_back(parse_rule(r));
                }
            }
            return rules;
        }
#endif  //C++17 or higher
    }
}

using namespace ns_test_filter_rule;
using namespace exfo::ut;

[[nodiscard]] test_stats test_is_include_single_imsi() {
    test_stats stats{};

    imsi_include_list_filter imsi_filt{{1010u}, {2020u}, {3030u}, {4040u}, {5050u}, {6060u}, {7070u}};

    T_CDR_MSG cdr2020{2020u, cdr_status::good, 20000, 30000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    stats += require(exfo::is_include(cdr2020, imsi_filt), "is_include<cdr, imsi_filt>", __FILE__, __LINE__, 0, __func__);

    T_CDR_MSG cdr8080{8080u, cdr_status::good, 20000, 30000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    stats += require(exfo::is_include(cdr8080, imsi_filt) == false, "NOT is_include<cdr, imsi_filt>", __FILE__, __LINE__, 0, __func__);

    return stats;
}

[[nodiscard]] test_stats test_is_include_single_cdr_status() {
    test_stats stats{};

    cdr_status_list_filter status_good{cdr_status::good};
    cdr_status_list_filter status_error{cdr_status::error};
    cdr_status_list_filter status_multiple{cdr_status::good, cdr_status::error};

    T_CDR_MSG cdr_good{2020u, cdr_status::good, 20000, 30000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr_error{8080u, cdr_status::error, 20000, 30000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};

    stats += require(exfo::is_include(cdr_good, status_good) == true, "is_include<cdr, status_good>", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr_error, status_good) == false, "NOT is_include<cdr, status_good>", __FILE__, __LINE__, 0, __func__);

    stats += require(exfo::is_include(cdr_good, status_error) == false, "NOT is_include<cdr, status_error>", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr_error, status_error) == true, "is_include<cdr, status_error>", __FILE__, __LINE__, 0, __func__);

    stats += require(exfo::is_include(cdr_good, status_multiple) == true, "is_include<cdr, status_multiple>", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr_error, status_multiple) == true, "is_include<cdr, status_multiple>", __FILE__, __LINE__, 0, __func__);

    return stats;
}

[[nodiscard]] test_stats test_is_include_two_ports() {
    test_stats stats{};

    port_include_list_filter port_filt_even{{20000}, {40000}, {60000}};
    port_include_list_filter port_filt_odd{{10000}, {30000}, {50000}};

    T_CDR_MSG cdr2030{2020u, cdr_status::good, 20000, 30000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr15{8080u, cdr_status::good, 1000, 5000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};

    stats += require(exfo::is_include(cdr2030, port_filt_even) == true, "is_include<cdr, port_filt> src_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr2030, port_filt_odd) == true, "is_include<cdr, port_filt> dst_port", __FILE__, __LINE__, 0, __func__);

    stats += require(exfo::is_include(cdr15, port_filt_even) == false, "NOT is_include<cdr, port_filt> src_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr15, port_filt_odd) == false, "NOT is_include<cdr, port_filt> dst_port", __FILE__, __LINE__, 0, __func__);

    return stats;
}

[[nodiscard]] test_stats test_is_include_many() {
    test_stats stats{};

    port_include_list_filter port_filt_even{{20000}, {40000}, {60000}};
    port_include_list_filter port_filt_odd{{10000}, {30000}, {50000}};
    std::vector<port_include_list_filter> filters = {port_filt_odd, port_filt_even};

    T_CDR_MSG cdr2030{2020u, cdr_status::good, 20000, 30000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr205{2020u, cdr_status::good, 20000, 5000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr15{8080u, cdr_status::good, 1000, 5000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};

    stats += require(exfo::is_include(cdr2030, filters) == true, "is_include<cdr, std::vector<port_filt>> src_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr205, filters) == true, "is_include<cdr, std::vector<port_filt>> src_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr15, filters) == false, "NOT is_include<cdr, std::vector<port_fil>t> src_port", __FILE__, __LINE__, 0, __func__);

    stats += require(exfo::is_include_any(cdr2030, filters) == true, "is_include_any<cdr, std::vector<port_fil>t> src_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr205, filters) == true, "is_include_any<cdr, std::vector<port_fil>t> src_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr15, filters) == false, "NOT is_include_any<cdr, std::vector<port_filt>> dst_port", __FILE__, __LINE__, 0, __func__);

    stats += require(exfo::is_include_all(cdr2030, filters) == true, "is_include_any<cdr, std::vector<port_fil>t> src_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr205, filters) == false, "NOT is_include_any<cdr, std::vector<port_filt>> dst_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr15, filters) == false, "NOT is_include_any<cdr, std::vector<port_filt>> dst_port", __FILE__, __LINE__, 0, __func__);

    std::vector<port_include_list_filter> empty_filters;
    stats += require(exfo::is_include(cdr2030, empty_filters) == true, "is_include<cdr, empty_filters> ports", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr205, empty_filters) == true, "is_include<cdr, empty_filters> ports", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr15, empty_filters) == true, "is_include<cdr, empty_filters> ports", __FILE__, __LINE__, 0, __func__);

    return stats;
}

[[nodiscard]] test_stats test_is_include_rule_t() {
    test_stats stats{};

#if __cplusplus >= 201703L

    exfo::rule_t<imsi_include_list_filter, port_include_list_filter> port_filt_even{port_include_list_filter{{20000}, {40000}, {60000}}};
    exfo::rule_t<imsi_include_list_filter, port_include_list_filter> port_filt_odd{port_include_list_filter{{10000}, {30000}, {50000}}};
    exfo::rule_t<imsi_include_list_filter, port_include_list_filter> imsi_filt{imsi_include_list_filter{{1010u}, {2020u}, {3030u}, {4040u}, {5050u}, {6060u}, {7070u}}};

    T_CDR_MSG cdr2030{2020u, cdr_status::good, 20000, 30000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr15{8080u, cdr_status::good, 1000, 5000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};

    stats += require(exfo::is_include(cdr2030, imsi_filt) == true, "is_include<cdr, rule_t<imsi_filt>>", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr2030, port_filt_even) == true, "is_include<cdr, rule_t<port_filt>> src_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr2030, port_filt_odd) == true, "is_include<cdr, rule_t<port_filt>> dst_port", __FILE__, __LINE__, 0, __func__);

    stats += require(exfo::is_include(cdr15, imsi_filt) == false, "NOT is_include<cdr, rule_t<imsi_filt>>", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr15, port_filt_even) == false, "NOT is_include<cdr, rule_t<port_filt>> src_port", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr15, port_filt_odd) == false, "NOT is_include<cdr, rule_t<port_filt>> dst_port", __FILE__, __LINE__, 0, __func__);

#endif  //C++17 or higher to use rule_t

    return stats;
}

[[nodiscard]] test_stats test_is_include_vector_rule_t() {
    test_stats stats{};

#if __cplusplus >= 201703L

    std::vector<exfo::rule_t<imsi_include_list_filter, port_include_list_filter, cdr_status_list_filter>> rules{
        port_include_list_filter{{20000}, {40000}, {60000}},
        port_include_list_filter{{10000}, {30000}, {50000}},
        imsi_include_list_filter{{1010u}, {2020u}, {3030u}, {4040u}, {5050u}, {6060u}, {7070u}},
        cdr_status_list_filter{cdr_status::error}
    };

    T_CDR_MSG cdr_match_0th{9090u, cdr_status::good, 20000, 30555, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr_match_1st{9090u, cdr_status::good, 20555, 30000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr_match_2nd{1010u, cdr_status::good, 20555, 30555, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr_match_3rd{9090u, cdr_status::error, 20555, 30555, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr_match_none{9090u, cdr_status::good, 20555, 30555, std::chrono::system_clock::now(), std::chrono::system_clock::now()};
    T_CDR_MSG cdr_match_all{4040u, cdr_status::error, 20000, 30000, std::chrono::system_clock::now(), std::chrono::system_clock::now()};

    stats += require(exfo::is_include(cdr_match_0th, rules) == true, "is_include<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 0th rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr_match_1st, rules) == true, "is_include<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 1st rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr_match_2nd, rules) == true, "is_include<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 2nd rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr_match_3rd, rules) == true, "is_include<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 3rd rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr_match_all, rules) == true, "is_include<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match ALL rules", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include(cdr_match_none, rules) == false, "is_include<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match NO rule", __FILE__, __LINE__, 0, __func__);

    stats += require(exfo::is_include_any(cdr_match_0th, rules) == true, "is_include_any<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 0th rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr_match_1st, rules) == true, "is_include_any<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 1st rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr_match_2nd, rules) == true, "is_include_any<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 2nd rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr_match_3rd, rules) == true, "is_include_any<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 3rd rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr_match_all, rules) == true, "is_include_any<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match ALL rules", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr_match_none, rules) == false, "is_include_any<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match NO rule", __FILE__, __LINE__, 0, __func__);

    stats += require(exfo::is_include_all(cdr_match_0th, rules) == false, "is_include_all<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 0th rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr_match_1st, rules) == false, "is_include_all<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 1st rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr_match_2nd, rules) == false, "is_include_all<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 2nd rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr_match_3rd, rules) == false, "is_include_all<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match 3rd rule", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr_match_all, rules) == true, "is_include_all<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match ALL rules", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr_match_none, rules) == false, "is_include_all<cdr, vector<rule_t<port_filt, imsi_filt, cdr_status_filt>>> match NO rule", __FILE__, __LINE__, 0, __func__);

    decltype(rules) empty_rules{};
    stats += require(exfo::is_include(cdr_match_none, empty_rules) == true, "is_include<cdr, empty_rules> ALWAYS true", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr_match_none, empty_rules) == true, "is_include<cdr, empty_rules> ALWAYS true", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr_match_none, empty_rules) == true, "is_include<cdr, empty_rules> ALWAYS true", __FILE__, __LINE__, 0, __func__);

    stats += require(exfo::is_include(cdr_match_2nd, empty_rules) == true, "is_include<cdr, empty_rules> ALWAYS true", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_any(cdr_match_2nd, empty_rules) == true, "is_include<cdr, empty_rules> ALWAYS true", __FILE__, __LINE__, 0, __func__);
    stats += require(exfo::is_include_all(cdr_match_2nd, empty_rules) == true, "is_include<cdr, empty_rules> ALWAYS true", __FILE__, __LINE__, 0, __func__);
#endif  //C++17 or higher to use std::variant

    return stats;
}

int main(int argc, char** argv) {
    test_stats stats{};

    stats += test_is_include_single_imsi();
    stats += test_is_include_single_cdr_status();
    stats += test_is_include_two_ports();
    stats += test_is_include_many();
    stats += test_is_include_rule_t();
    stats += test_is_include_vector_rule_t();

    if (stats.failed) {
        std::cout << "FAILED tests = " << stats.failed << "; Total tests = " << stats.total << '\n';
    }
    return -stats.failed;
}