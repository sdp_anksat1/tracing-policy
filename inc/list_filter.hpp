#ifndef EXFO_LIST_FILTER_HPP
#define EXFO_LIST_FILTER_HPP

#include "algorithm.hpp"

#include <vector>
#include <algorithm>
#include <functional>

namespace exfo
{
    template<typename T, int no_values = 1, bool is_include_filter = true>
    struct [[nodiscard]] list_filter
    {
        using value_type = T;
        static const int values_count = no_values;

        constexpr list_filter(std::initializer_list<T> vals) : values(sort_dedup(vals)) {}
        list_filter(std::vector<T>&& vals) : values(sort_dedup(std::move(vals))) {}

        template<typename Iter>
        constexpr list_filter(Iter b, Iter e) : values(sort_dedup({b, e})) {}
        constexpr list_filter() = default;

        [[nodiscard]] constexpr const std::vector<T>& get_list() const noexcept { return values; }
        [[nodiscard]] constexpr const std::vector<T>& get_include_list() const noexcept;
        [[nodiscard]] constexpr const std::vector<T>& get_exclude_list() const noexcept;

        [[nodiscard]] constexpr bool should_include(const T& value) const noexcept;

        private:
        [[nodiscard]] constexpr bool find(const T& value) const noexcept {
            return std::binary_search(begin(values), end(values), value);
        }
        [[nodiscard]] static std::vector<T> sort_dedup(std::vector<T> vals) {
            std::sort(begin(vals), end(vals));
            exfo::dedup_sorted(vals);
            return vals;
        }

        std::vector<T> values;
        const std::vector<T> empty_vals{};  //Not static to not need initialization in .cpp if std < C++17
    };

    #if __cplusplus >= 201703L
    template<typename Iter>
    list_filter(Iter b, Iter e) -> list_filter<typename std::iterator_traits<Iter>::value_type>;
    #endif

    template<typename T, int no_values = 1>
    using include_list_filter = list_filter<T, no_values, true>;

    template<typename T, int no_values = 1>
    using exclude_list_filter = list_filter<T, no_values, false>;

    template<typename T, int no_values = 1>
    struct include_exclude_list_filter
    {
        using value_type = T;
        static thread_local const int values_count = no_values;

        include_exclude_list_filter(std::initializer_list<T> include_values,
            std::initializer_list<T> exclude_values) : 
            include_filter{include_values}, exclude_filter{exclude_values}
            {}

        [[nodiscard]] std::pair<const std::vector<T>&, const std::vector<T>&> get_list() const noexcept {
            return std::make_pair(std::ref(include_filter.get_include_list()), std::ref(exclude_filter.get_exclude_list()));
        }
        [[nodiscard]] const std::vector<T>& get_include_list() const noexcept { return include_filter.get_include_list(); }
        [[nodiscard]] const std::vector<T>& get_exclude_list() const noexcept { return exclude_filter.get_exclude_list(); }

        [[nodiscard]] bool should_include(const T& value) const noexcept {
            return include_filter.should_include(value) || exclude_filter.should_include(value);
        }

        private:
        include_list_filter<T, no_values> include_filter;
        exclude_list_filter<T, no_values> exclude_filter;
    };

    template<typename T, int no_values, bool is_include_filter>
    [[nodiscard]] constexpr bool list_filter<T, no_values, is_include_filter>::should_include(const T& value) const noexcept {
        #if __cplusplus >= 201703L
        if constexpr (is_include_filter) {
            return values.empty()? false: find(value);  //empty include list means include nothing i.e. should_include=false
        } else {
            return values.empty()? true: !find(value);  //empty exclude list means exclude nothing i.e. should_include=true
        }
        #else
        return is_include_filter?
            (values.empty()? false: find(value)):  //empty include list means include nothing i.e. should_include=false
            (values.empty()? true: !find(value));  //empty exclude list means exclude nothing i.e. should_include=true
        #endif
    }
    template<typename T, int no_values, bool is_include_filter>
    [[nodiscard]] constexpr const std::vector<T>& list_filter<T, no_values, is_include_filter>::get_include_list() const noexcept {
        #if __cplusplus >= 201703L
        if constexpr (is_include_filter) { return values; }
        else { return empty_vals; }
        #else
        return is_include_filter? values: empty_vals;
        #endif
    }
    template<typename T, int no_values, bool is_include_filter>
    [[nodiscard]] constexpr const std::vector<T>& list_filter<T, no_values, is_include_filter>::get_exclude_list() const noexcept {
        #if __cplusplus >= 201703L
        if constexpr (is_include_filter) { return empty_vals; }
        else { return values; }
        #else
        return is_include_filter? empty_vals: values;
        #endif
    }
}

#endif //EXFO_LIST_FILTER_HPP