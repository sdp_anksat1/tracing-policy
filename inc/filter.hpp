#ifndef EXFO_FILTER_HPP
#define EXFO_FILTER_HPP

#include <vector>
#include <algorithm>
#include <concepts>
#include <variant>

namespace exfo
{
#if __cpp_concepts

    template<typename RuleType>
    concept rule = requires (RuleType rule) {
        typename RuleType::value_type;
        RuleType::values_count;
    };

    template<typename EventType, typename ValueType>
    concept event_has_value_type = requires (EventType event) {
        { get_val<ValueType>(event) } -> std::same_as<ValueType>;
    };

    template<typename EventType, typename ValueType, int N>
    concept event_has_nth_value_type = requires (EventType event) {
        { get_val<ValueType, N - 1>(event) } -> std::same_as<ValueType>;
    };

    /*template<typename EventType, typename ValueType, int N>
    concept event_has_n_value_types = requires {
        N >= 1;
        event_has_nth_value_type<EventType, ValueType, N>;
        event_has_n_value_types<EventType, ValueType, N - 1>; //TODO
    }
    || requires { N == 0; };*/

#endif  //__cpp_concepts

#if __cplusplus < 202002L
    //Just to make the compiler happy when using C++17 or lower
    //Fixed by P0846R0 in C++20 https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0846r0.html
    template<typename EventType, typename ValueType>
    [[nodiscard]] const ValueType& get_val(const EventType&);
#endif  //lower than C++20

    /*template<typename RuleType>
    //requires rule<RuleType>
    bool is_include(const RuleType& r, const typename RuleType::value_type& val) {
        return r.should_include(val);
    }*/

    template<typename EventType, typename RuleType>
    //requires rule<RuleType>
        //&& (RuleType::values_count == 1 && event_has_value_type<EventType, typename RuleType::value_type>)
        //&& (RuleType::values_count >= 1 &&
            //event_has_n_value_types<EventType, typename RuleType::value_type, RuleType::values_count>)
    [[nodiscard]] bool is_include(const EventType& event, const RuleType& rule) {
        #if __cpp_if_constexpr
        if constexpr (rule.values_count > 1)
        #else
        if (rule.values_count > 1)
        #endif
        {
            bool matched = false;
            for (int i = 0; !matched && i < rule.values_count; ++i) {
                matched |= rule.should_include(get_val<typename RuleType::value_type>(event, i));
            }
            return matched;
        } else {
            return rule.should_include(get_val<typename RuleType::value_type>(event)); //get_val<imsi>(t_cdr_info);
        }
    }

    template<typename EventType, typename RuleType>
    //requires rule<RuleType>   ??
    [[nodiscard]] bool is_include_any(const EventType& event, const std::vector<RuleType>& rules) {
        if (rules.empty()) { return true; } //No filtering

        bool should_include = false;
        for (auto it = begin(rules); it != end(rules) && !should_include; ++it) {
            should_include |= is_include(event, *it);
        }
        return should_include;
    }

    template<typename EventType, typename RuleType>
    //requires rule<RuleType>   ??
    [[nodiscard]] bool is_include_all(const EventType& event, const std::vector<RuleType>& rules) {
        if (rules.empty()) { return true; } //No filtering

        bool should_include = true;
        for (auto it = begin(rules); it != end(rules) && should_include; ++it) {
            should_include &= is_include(event, *it);
        }
        return should_include;
    }

    template<typename EventType, typename RuleType>
    //requires rule<RuleType>   ??
    [[nodiscard]] bool is_include(const EventType& event, const std::vector<RuleType>& rules) {
        return is_include_any(event, rules);
    }

#if __cplusplus >= 201703L

    template<typename... Types>
    using rule_t = std::variant<Types...>;

    template<typename EventType, typename... RuleTypes>
    //requires Ruletypes... to all model concept rule
    [[nodiscard]] bool is_include(const EventType& event, const rule_t<RuleTypes...>& rule) {
        return std::visit([&event](const auto& r) { return is_include(event, r); }, rule);
    }

    template<typename EventType, typename... Rules>
    //requires rule<Rule>   ??
    [[nodiscard]] bool is_include_any(const EventType& event, const std::vector<rule_t<Rules...>>& rules) {
        if (rules.empty()) { return true; } //No filtering

        bool should_include = false;
        for (auto it = begin(rules); it != end(rules) && !should_include; ++it) {
            should_include |= std::visit([&event](const auto& r) { return is_include(event, r); }, *it);
        }
        return should_include;
    }

    template<typename EventType, typename... Rules>
    //requires rule<Rule>   ??
    [[nodiscard]] bool is_include_all(const EventType& event, const std::vector<rule_t<Rules...>>& rules) {
        if (rules.empty()) { return true; } //No filtering

        bool should_include = true;
        for (auto it = begin(rules); it != end(rules) && should_include; ++it) {
            should_include &= std::visit([&event](const auto& r) { return is_include(event, r); }, *it);
        }
        return should_include;
    }

    template<typename EventType, typename... Rules>
    //requires rule<Rule>   ??
    [[nodiscard]] bool is_include(const EventType& event, const std::vector<rule_t<Rules...>>& rules) {
        return is_include_any(event, rules);
    }

#endif  //C++17 or higher
}

#endif //EXFO_FILTER_HPP